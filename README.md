# Ud6-Ejemplo7
_Ejemplo 7 de la Unidad 6._

En este ejemplo vamos a basarnos en el anterior([Ud6-Ejemplo6](https://bitbucket.org/bitbucketcarlos/ud6-ejemplo6)) y vamos a implementar la misma aplicación pero en este caso 
el modo noche se activará al pulsar el elemento _SwitchPreference_.
Todos los ficheros serán idénticos salvo los siguientes:

## _ActividadPreferencias_

Ahora la actividad _ActividadPreferencias_ solo realizará el reemplazo del _Fragment_ _PreferenciasFragment_. No cambiará el modo elegido.

```java
public class ActividadPreferencias extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Creamos la transacción y añadimos el PreferenceFragment creado
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferenciasFragment())
                .commit();
    }
}
```

## _PreferenciasFragment_

Será el _Fragment_ _PreferenciasFragment_ quien compruebe si el elemento _SwitchPreference_ ha cambiado de valor mediante el _Listener_ _OnPreferenceChangeListener_.

```java
public class PreferenciasFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Añadimos las preferencias creadas en el fichero "preferencias.xml"
        addPreferencesFromResource(R.xml.preferencias);

        // Controlamos si el elemento SwitchPreference cambia
        SwitchPreference pref = findPreference("ModoNoche");

        pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                if (newValue.toString().equals("true"))
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                else
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

                return true;
            }
        });
    }
}
```