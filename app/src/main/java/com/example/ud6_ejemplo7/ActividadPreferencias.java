package com.example.ud6_ejemplo7;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class ActividadPreferencias extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Creamos la transacción y añadimos el PreferenceFragment creado
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferenciasFragment())
                .commit();
    }
}