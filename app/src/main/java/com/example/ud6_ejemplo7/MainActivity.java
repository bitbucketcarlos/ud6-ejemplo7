package com.example.ud6_ejemplo7;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtenermos las preferencias almacenadas
        SharedPreferences prefe = PreferenceManager.getDefaultSharedPreferences(this);
        boolean modoNoche = prefe.getBoolean("ModoNoche", false);

        if(modoNoche)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // "Inflamos" el menú diseñado
        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Si se pulsa sobre el item "Preferencias" abrimos la actividad ActividadPreferencias
        if(item.getItemId() == R.id.preferencias){
            Intent intentPref = new Intent(this, ActividadPreferencias.class);
            startActivity(intentPref);

            return true;
        }
        return super.onContextItemSelected(item);
    }
}